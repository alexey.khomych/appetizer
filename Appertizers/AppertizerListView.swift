//
//  AppertizerListView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 03.10.2023.
//

import SwiftUI

struct AppertizerListView: View {
    
    @StateObject var viewModel = AppetizerListViewModel()
    
    var body: some View {
        ZStack {
            NavigationView {
                List(viewModel.appertizers) { appertizer in
                    AppertizerItemView(item: appertizer)
                        .listRowSeparatorTint(.brandPrimary)
                        .onTapGesture {
                            viewModel.selectedAppetizer = appertizer
                            viewModel.isShowingDetail = true
                        }
                }
                .navigationTitle("Appertizers")
                .listStyle(.plain)
                .disabled(viewModel.isShowingDetail)
            }
            .task {
                await viewModel.getAppertizers()
            }
            .blur(radius: viewModel.isShowingDetail ? 20.0 : 0)
            
            if viewModel.isShowingDetail {
                AppetizerDetailView(
                    item: viewModel.selectedAppetizer!,
                    isShowingDetail: $viewModel.isShowingDetail
                )
            }
            
            if viewModel.isLoading {
                LoadingView()
            }
        }
        .alert(item: $viewModel.alertItem) { item in
            Alert(title: item.title, message: item.message, dismissButton: item.dismissButton)
        }
    }
}

#Preview {
    AppertizerListView()
}
