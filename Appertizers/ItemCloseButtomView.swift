//
//  OrderButtonView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 09.10.2023.
//

import SwiftUI

struct ItemCloseButtomView: View {
    
    @Binding var isShowingDetail: Bool
    
    var body: some View {
        Button {
            isShowingDetail = false
        } label: {
            ZStack {
                Circle()
                    .frame(width: 30, height: 30)
                    .foregroundColor(.white)
                    .opacity(0.6)
                
                Image(systemName: "xmark")
                    .imageScale(.medium)
                    .frame(width: 44, height: 44)
                    .foregroundColor(.black)
            }
        }
    }
}

#Preview {
    ItemCloseButtomView(isShowingDetail: .constant(false))
}
