//
//  Appertizer.swift
//  Appertizers
//
//  Created by Alexey Khomych on 03.10.2023.
//

import Foundation

struct Appertizer: Decodable, Identifiable {
    let id: Int
    let name: String
    let description: String
    let price: Double
    let imageURL: String
    let calories: Int
    let protein: Int
    let carbs: Int
}

struct AppertizerResponse: Decodable {
    let request: [Appertizer]
}

struct MockData {
    static let sample = Appertizer(id: 0001, name: "Test Apper", description: "This is a descr for my apper This is a descr for my apper This is a descr for my apper This is a descr for my apper", price: 101, imageURL: "asian-flank-steak", calories: 400, protein: 100, carbs: 333)
    static let sample1 = Appertizer(id: 0002, name: "Test Apper", description: "This is a descr for my apper This is a descr for my apper This is a descr for my apper This is a descr for my apper", price: 101, imageURL: "asian-flank-steak", calories: 400, protein: 100, carbs: 333)
    static let sample2 = Appertizer(id: 0003, name: "Test Apper", description: "This is a descr for my apper This is a descr for my apper This is a descr for my apper This is a descr for my apper", price: 101, imageURL: "asian-flank-steak", calories: 400, protein: 100, carbs: 333)
    static let samples = [sample, sample1, sample2]
}
