//
//  OrderView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 03.10.2023.
//

import SwiftUI

struct OrderView: View {

    @EnvironmentObject var order: Order
    
    @StateObject var viewModel = OrderViewModel()
    
    private var price = 0.0
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    List {
                        ForEach(order.items) { appetizer in
                            AppertizerItemView(item: appetizer)
                        }
                        .onDelete(perform: { indexSet in
                            deleteItem(at: indexSet)
                        })
                    }
                    .listStyle(.plain)
                    
                    ConfirmOrderButtonView(
                        isConfirmed: $viewModel.isShowingItem,
                        price: order.sum,
                        specifier: "%.2f"
                    )
                }
                
                if order.items.isEmpty {
                    EmptyStateView(
                        description: "You have no items in your order.",
                        imageName: "empty-order"
                    )
                }
            }
            .navigationTitle("Orders")
        }
    }
    
    func deleteItem(at offsets: IndexSet) {
        order.items.remove(atOffsets: offsets)
    }
}

#Preview {
    OrderView()
}
