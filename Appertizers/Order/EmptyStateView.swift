//
//  EmptyStateView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 10.10.2023.
//

import SwiftUI

struct EmptyStateView: View {
    
    let description: String
    let imageName: String
    
    var body: some View {
        ZStack {
            Color(.systemBackground)
                .ignoresSafeArea(.all)
            VStack {
                Image(imageName)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 150, height: 150)
                Text(description)
                    .font(.body)
                    .foregroundStyle(.secondary)
                    .multilineTextAlignment(.center)
                    .padding()
            }
            .offset(y: -50)
        }
    }
}

#Preview {
    EmptyStateView(description: "You have no items in your order.", imageName: "empty-order")
}
