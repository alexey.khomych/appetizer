//
//  ConfirmOrderButtonView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 09.10.2023.
//

import SwiftUI

struct ConfirmOrderButtonView: View {
    
    @Binding var isConfirmed: Bool
    let price: Double
    let specifier: String
    
    var body: some View {
        Button(action: {
            isConfirmed = false
        }, label: {
            Text("$\(price, specifier: specifier) - Place Order")
                .frame(height: 40)
        })
        .modifier(StandartButtonStyle())
        .padding(.bottom, 30)
    }
}

#Preview {
    ConfirmOrderButtonView(isConfirmed: .constant(false), price: 0.0, specifier: "%.2f")
}
