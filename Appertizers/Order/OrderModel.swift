//
//  OrderModel.swift
//  Appertizers
//
//  Created by Alexey Khomych on 09.10.2023.
//

import SwiftUI

final class Order: ObservableObject {
    
    @Published var items: [Appertizer] = []
    
    var sum: Double {
        items.reduce(0) { $0 + $1.price }
    }
    
    var badgeCount: Int {
        items.count
    }
    
    func add(_ item: Appertizer) {
        items.append(item)
    }
}
