//
//  OrderViewModel.swift
//  Appertizers
//
//  Created by Alexey Khomych on 09.10.2023.
//

import SwiftUI

final class OrderViewModel: ObservableObject {
    
    @Published var isShowingItem = false
    @Published var order = Order()
}
