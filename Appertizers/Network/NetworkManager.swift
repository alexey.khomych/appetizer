//
//  NetworkManager.swift
//  Appertizers
//
//  Created by Alexey Khomych on 03.10.2023.
//

import UIKit

final class NetworkManager {
    
    static let shared = NetworkManager()
    
    static let baseURL = "https://seanallen-course-backend.herokuapp.com/swiftui-fundamentals/"
    static let appetizerURL = baseURL + "appetizers"
    
    private let cache = NSCache<NSString, UIImage>()
    
    private init() {
        
    }
    
//    func getAppertizers(completed: @escaping (Result<[Appertizer], APIError>) -> Void) {
//        guard let url = URL(string: NetworkManager.appetizerURL) else {
//            completed(.failure(.invalidURL))
//            return
//        }
//        
//        let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { data, response, error in
//            if error != nil {
//                completed(.failure(.unableToComplete))
//                
//                return
//            }
//            
//            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
//                completed(.failure(.invalidResponse))
//                
//                return
//            }
//            
//            guard let data else {
//                completed(.failure(.invalidData))
//                
//                return
//            }
//            
//            do {
//                let decoder = JSONDecoder()
//                let decoderResponse = try decoder.decode(AppertizerResponse.self, from: data)
//                completed(.success(decoderResponse.request))
//            } catch {
//                completed(.failure(.invalidData))
//            }
//        }
//        
//        task.resume()
//    }
    
    func getAppertizers() async throws -> [Appertizer] {
        guard let url = URL(string: NetworkManager.appetizerURL) else {
            throw APIError.invalidURL
        }
        
        let (data, _) = try await URLSession.shared.data(from: url)
        
        do {
            let decoder = JSONDecoder()
            let decoderResponse = try decoder.decode(AppertizerResponse.self, from: data)
            return decoderResponse.request
        } catch {
            throw APIError.invalidData
        }
    }
    
    func downloadImage(fromURLString urlString: String, completed: @escaping (UIImage?) -> Void) {
        let cacheKey = NSString(string: urlString)
        
        if let image = cache.object(forKey: cacheKey) {
            completed(image)
            
            return
        }
        
        guard let url = URL(string: urlString) else {
            completed(nil)
            
            return
        }
        
        let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { [self] data, response, _ in
            guard let data, let image = UIImage(data: data) else {
                completed(nil)
                return
            }
            
            cache.setObject(image, forKey: cacheKey)
            completed(image)
        }
        
        task.resume()
    }
}

enum APIError: Error {
    case invalidURL
    case invalidResponse
    case invalidData
    case unableToComplete
}
