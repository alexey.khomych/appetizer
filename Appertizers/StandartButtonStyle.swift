//
//  StandartButtonStyle.swift
//  Appertizers
//
//  Created by Alexey Khomych on 10.10.2023.
//

import SwiftUI

struct StandartButtonStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .foregroundStyle(.white)
            .buttonStyle(.bordered)
            .tint(.brandPrimary)
            .controlSize(.large)
    }
}
