//
//  OrderButtonView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 09.10.2023.
//

import SwiftUI

struct OrderButtonView: View {
    
    let price: Double
    let specifier: String
    let action: (() -> Void)?
    
    var body: some View {
        Button(action: {
            action?()
        }, label: {
            Text("$\(price, specifier: specifier) - Add To Order")
//                .frame(height: 40)
        })
        .modifier(StandartButtonStyle())
        .padding(.bottom, 30)
    }
}

#Preview {
    OrderButtonView(price: 10.0, specifier: "%.2f", action: nil)
}
