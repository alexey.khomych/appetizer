//
//  AppetizerDetailView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 05.10.2023.
//

import SwiftUI

struct AppetizerDetailView: View {
    
    let item: Appertizer
    @Binding var isShowingDetail: Bool
    
    @EnvironmentObject var order: Order
    
    var body: some View {
        VStack {
            AppertizerRemoteImage(urlString: item.imageURL)
                .aspectRatio(contentMode: .fit)
                .frame(width: 300, height: 225)

            Text(item.name)
                .font(.title)
                .bold()
            
            Text(item.description)
                .lineLimit(2, reservesSpace: true)
                .padding()
            
            HStack {
                AppetizerItemCompoundView(title: "Calories", value: item.calories, attribute: nil)
                AppetizerItemCompoundView(title: "Carbs", value: item.carbs, attribute: "g")
                AppetizerItemCompoundView(title: "Protein", value: item.protein, attribute: "g")
            }
            
            Spacer()
            
            OrderButtonView(price: item.price, specifier: "%.2f") {
                isShowingDetail.toggle()
                order.add(item)
            }
        }
        .frame(width: 300, height: 525)
        .background(Color(.systemBackground))
        .clipShape(.rect(cornerRadius: 20, style: .circular))
        .shadow(radius: 40)
        .overlay(ItemCloseButtomView(isShowingDetail: $isShowingDetail), alignment: .topTrailing)
    }
}

#Preview {
    AppetizerDetailView(item: MockData.sample, isShowingDetail: .constant(true))
}
