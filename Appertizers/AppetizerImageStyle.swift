//
//  AppetizerImageStyle.swift
//  Appertizers
//
//  Created by Alexey Khomych on 10.10.2023.
//

import SwiftUI

protocol ImageModifier {
    associatedtype Body: View
    
    func body(image: Image) -> Self.Body
}

extension Image {
    func modifier<M>(_ modifier: M) -> some View where M: ImageModifier {
        modifier.body(image: self)
    }
}

struct AppetizerImageStyle: ImageModifier {
    
    func body(image: Image) -> some View {
        image
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 120, height: 90)
            .clipShape(.rect(cornerRadius: 10))
    }
}
