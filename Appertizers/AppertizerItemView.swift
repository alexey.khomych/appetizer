//
//  AppertizerItemView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 03.10.2023.
//

import SwiftUI

struct AppertizerItemView: View {
    
    @State var item: Appertizer
    
    var body: some View {
        HStack(spacing: 15) {
            AsyncImage(url: URL(string: item.imageURL)) { image in
                image
                    .modifier(AppetizerImageStyle())
            } placeholder: {
                Image("food-placeholder")
                    .modifier(AppetizerImageStyle())
            }
            
            VStack(alignment: .leading, spacing: 5) {
                Text(item.name)
                    .font(.system(size: 20))
                Text(item.description)
                    .font(.system(size: 16))
                Text("$\(item.price, specifier: "%.2f")")
                    .font(.system(size: 14))
                    .fontWeight(.semibold)
                    .foregroundStyle(.secondary)
            }
        }
        
    }
}

#Preview {
    AppertizerItemView(item: MockData.sample)
}
