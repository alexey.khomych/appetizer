//
//  ContentView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 22.09.2023.
//

import SwiftUI

struct AppertizerTabView: View {
    
    @EnvironmentObject var order: Order
    
    var body: some View {
        TabView {
            AppertizerListView()
                .tabItem {
                    Label("Home", systemImage: "house")
                }
            AccountView(account: AccountModel.mock)
                .tabItem {
                    Label("Account", systemImage: "person")
                }
            OrderView()
                .tabItem {
                    Label("Order", systemImage: "bag")
                }
                .badge(order.badgeCount)
        }
        .accentColor(.brandPrimary)
        .foregroundStyle(.secondary)
    }
}

#Preview {
    AppertizerTabView()
}
