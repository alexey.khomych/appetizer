//
//  Alert.swift
//  Appertizers
//
//  Created by Alexey Khomych on 04.10.2023.
//

import SwiftUI

struct AlertItem: Identifiable {
    let id = UUID()
    let title: Text
    let message: Text
    let dismissButton: Alert.Button
}
