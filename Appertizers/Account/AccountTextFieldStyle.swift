//
//  AccountTextFieldStyle.swift
//  Appertizers
//
//  Created by Alexey Khomych on 10.10.2023.
//

import SwiftUI

struct AccountTextFieldStyle: ViewModifier {
    
    func body(content: Content) -> some View {
        content
            .foregroundStyle(.primary)
            .bold()
    }
}
