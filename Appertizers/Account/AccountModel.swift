//
//  AccountModel.swift
//  Appertizers
//
//  Created by Alexey Khomych on 09.10.2023.
//

import Foundation

struct AccountModel: Codable {
    var firstName: String = ""
    var lastName: String = ""
    var email: String = ""
    var birthdate: Date = Date()
    var napkins: Bool = false
    var refills: Bool = false
}

extension AccountModel {
    static let mock = AccountModel(firstName: "Alex", lastName: "", email: "", birthdate: Date(), napkins: false, refills: false)
}
