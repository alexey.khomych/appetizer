//
//  AccountView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 09.10.2023.
//

import SwiftUI

struct AccountView: View {
    
    let account: AccountModel
    
    @StateObject var viewModel = AccountViewModel()
    
    enum FormTextField {
        case firstName, lastName, email
    }
    
    @FocusState private var focusedTextField: FormTextField?

    var body: some View {
        Form {
            Section("PERSONAL INFO") {
                TextField("First Name", text: $viewModel.user.firstName)
                    .focused($focusedTextField, equals: .firstName)
                    .onSubmit {
                        focusedTextField = .lastName
                    }
                    .modifier(AccountTextFieldStyle())
                
                TextField("Last Name", text: $viewModel.user.lastName)
                    .focused($focusedTextField, equals: .lastName)
                    .onSubmit {
                        focusedTextField = .email
                    }
                    .modifier(AccountTextFieldStyle())
                
                TextField("Email", text: $viewModel.user.email)
                    .focused($focusedTextField, equals: .email)
                    .onSubmit {
                        focusedTextField = nil
                    }
                    .modifier(AccountTextFieldStyle())
                    .keyboardType(.emailAddress)
                    .textInputAutocapitalization(.none)
                    .autocorrectionDisabled()
                
                DatePicker(
                    "Birthday", 
                    selection: $viewModel.user.birthdate,
                    in: Date().oneHundredTenYearsAgo...Date().eighteenYearsAgo,
                    displayedComponents: .date
                )
                
                Button {
                    viewModel.saveChanges()
                } label: {
                    Text("Save Changes")
                }
            }
            
            Section("REQUESTS") {
                Toggle("Extra Napkins", isOn: $viewModel.user.napkins)
                Toggle("Frequent Refills", isOn: $viewModel.user.refills)
            }
            .toggleStyle(SwitchToggleStyle(tint: .brandPrimary))
        }
        .navigationTitle("Account")
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                Button("Dismiss") {
                    focusedTextField = nil
                }
            }
        }
        .onAppear(perform: {
            viewModel.retrieveUser()
        })
        .alert(item: $viewModel.alertItem) { item in
            Alert(title: item.title, message: item.message, dismissButton: item.dismissButton)
        }
    }
}

#Preview {
    AccountView(account: AccountModel.mock)
}

extension Date {
    
    var eighteenYearsAgo: Date {
        Calendar.current.date(byAdding: .year, value: -18, to: Date())!
    }
    
    var oneHundredTenYearsAgo: Date {
        Calendar.current.date(byAdding: .year, value: -110, to: Date())!
    }
}
