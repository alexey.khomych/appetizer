//
//  AccountViewModel.swift
//  Appertizers
//
//  Created by Alexey Khomych on 09.10.2023.
//

import SwiftUI

final class AccountViewModel: ObservableObject {
    
    @AppStorage("user") private var userData: Data?
    
    @Published var user = AccountModel()
    @Published var alertItem: AlertItem?
    
    var isValidForm: Bool {
        guard !user.firstName.isEmpty && !user.lastName.isEmpty && !user.email.isEmpty else {
            alertItem = AlertItem(title: Text("Some title"), message: Text("Some field is invalid"), dismissButton: .default(Text("ok")))
            return false
        }
        
        guard user.email.contains("@") else {
            alertItem = AlertItem(title: Text("Some title"), message: Text("Email is invalid"), dismissButton: .default(Text("ok")))
            return false
        }
        
        return true
    }
    
    func saveChanges() {
        guard isValidForm else {
            return
        }
        
        do {
            let data = try JSONEncoder().encode(user)
            userData = data
        } catch {
            alertItem = AlertItem(title: Text("Some title"), message: Text("Get error while saving"), dismissButton: .default(Text("ok")))
        }
        
        alertItem = AlertItem(title: Text("Some title"), message: Text("Saved"), dismissButton: .default(Text("ok")))
    }
    
    func retrieveUser() {
        do {
            guard let userData else {
                return
            }
            
            user = try JSONDecoder().decode(AccountModel.self, from: userData)
        } catch {
            alertItem = AlertItem(title: Text("Some title"), message: Text("Get error while loading"), dismissButton: .default(Text("ok")))
        }
    }
}
