//
//  AppetizerListViewModel.swift
//  Appertizers
//
//  Created by Alexey Khomych on 04.10.2023.
//

import SwiftUI

final class AppetizerListViewModel: ObservableObject {
    
    @Published var appertizers: [Appertizer] = []
    @Published var alertItem: AlertItem?
    @Published var isLoading = false
    @Published var isShowingDetail = false
    @Published var selectedAppetizer: Appertizer?
    
    @MainActor
    func getAppertizers() async {
        isLoading.toggle()
        
        Task {
            do {
                appertizers = try await NetworkManager.shared.getAppertizers()
            } catch {
                guard let error = error as? APIError else {
                    alertItem = AlertItem(title: Text("Some title"), message: Text("Some error"), dismissButton: .default(Text("ok")))
                    
                    return
                }
                
                switch error {
                    case .invalidData:
                        alertItem = AlertItem(title: Text("Some title"), message: Text("Some error"), dismissButton: .default(Text("ok")))
                    case .invalidResponse:
                        alertItem = AlertItem(title: Text("Some title"), message: Text("Some error"), dismissButton: .default(Text("ok")))
                    case .invalidURL:
                        alertItem = AlertItem(title: Text("Some title"), message: Text("Some error"), dismissButton: .default(Text("ok")))
                    case .unableToComplete:
                        alertItem = AlertItem(title: Text("Some title"), message: Text("Some error"), dismissButton: .default(Text("ok")))
                }
            }
            
            isLoading.toggle()
        }
    }
}
