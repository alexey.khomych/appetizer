//
//  AppertizersApp.swift
//  Appertizers
//
//  Created by Alexey Khomych on 22.09.2023.
//

import SwiftUI

@main
struct AppertizersApp: App {
    
    var order = Order()
    
    var body: some Scene {
        WindowGroup {
            AppertizerTabView().environmentObject(order)
        }
    }
}
