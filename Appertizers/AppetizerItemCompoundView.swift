//
//  AppetizerItemCompoundView.swift
//  Appertizers
//
//  Created by Alexey Khomych on 05.10.2023.
//

import SwiftUI

struct AppetizerItemCompoundView: View {
    
    let title: String
    let value: Int
    let attribute: String?
    
    var body: some View {
        VStack {
            Text(title)
                .font(.headline)
            Text("\(value) \(attribute ?? "")")
                .font(.footnote)
                .italic()
                .foregroundStyle(.gray)
        }
        .padding()
    }
}

#Preview {
    AppetizerItemCompoundView(title: "Calories", value: 800, attribute: "g")
}
